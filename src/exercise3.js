/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */
const fderive = (fn, h) => throw new Error('Not implemented');

const squear = x => throw new Error('Not implemented');

module.exports = {
  fderive,
  squear,
};
